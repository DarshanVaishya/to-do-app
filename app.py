from flask import *
import pymysql
from datetime import datetime

app = Flask(__name__)

@app.route('/')
def loadIndex():
	try:
		connection = pymysql.connect(
			host='localhost',
			user='root',
			password='',
			db='todo-app'
		)
		cur = connection.cursor()
		
		cur.execute("SELECT * FROM tasks ORDER BY date DESC")
		data = cur.fetchall()
		
		cur.close()
		connection.close()
		
		return render_template('index.html', data=data)
	except:
		return render_template('index.html', data="Error")

@app.route('/addTask', methods=["POST"])
def addTask():	
	task = request.form['task']
	date = str(datetime.now())[0:-7]
	connection = pymysql.connect(
		host='localhost',
		user='root',
		password='',
		db='todo-app'
	)
	cur = connection.cursor()
	cur.execute(f"INSERT INTO tasks(task, date) values('{task}', '{date}');")
	connection.commit()
	cur.close()
	connection.close()
	return redirect('/')


@app.route('/deleteTask', methods=["GET"])
def deleteTask():
	id = request.args.get('id')	
	connection = pymysql.connect(
		host='localhost',
		user='root',
		password='',
		db='todo-app'
	)
	cur = connection.cursor()
	
	cur.execute(f"DELETE FROM tasks where id={id}")
	connection.commit()
	cur.close()
	connection.close()
	return redirect('/')

@app.route('/stopApp')
def stopApp():
	func = request.environ.get('werkzeug.server.shutdown')
	if func is None:
		raise RuntimeError('Not running with the Werkzeug Server')
	func()	
	return "App is closed!"

if __name__ == '__main__':
	app.run()
